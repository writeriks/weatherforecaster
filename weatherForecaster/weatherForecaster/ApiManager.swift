//
//  ApiManager.swift
//  weatherForecaster
//
//  Created by Emir haktan Ozturk on 01/08/2017.
//  Copyright © 2017 emirhaktan. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class ApiManager{
    
    func loadData(urlString:String, completion:@escaping(_ error:Error?, _ weatherInfo:weatherModel?) -> Void)  {
        
        Alamofire.request(urlString).responseJSON  { response in
            
            if response.result.isSuccess{
            if let jsn = response.result.value{

                let json = JSON(jsn)//Swifty JSON Begins
                let model = weatherModel(json: json)

                for item in model.date{
                    print(item)
                    let date = NSDate(timeIntervalSince1970: item)
                    print(date)
                }

                  completion(nil,model)
              }
            }else if response.result.isFailure{
                print("Error")
                }
        }
    }
    
    func loadCountryData(completion:@escaping(_ error:Error?, _ countryInfo:countryCodeModel?) -> Void){
        do {
            if let file = Bundle.main.url(forResource: "countryCode", withExtension: "json") {
                let data = try Data(contentsOf: file)
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                    let jsn = JSON(json)
                
                    let countryModel = countryCodeModel(json: jsn)
                    completion(nil,countryModel)
            } else {
                print("no file")
            }
        } catch {
            print(error.localizedDescription)
        }
    }
}
    
