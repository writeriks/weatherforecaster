//
//  weatherModel.swift
//  weatherForecaster
//
//  Created by Emir haktan Ozturk on 01/08/2017.
//  Copyright © 2017 emirhaktan. All rights reserved.
//

import Foundation
import SwiftyJSON

class weatherModel{
    
    fileprivate var _vCityName :String
    fileprivate var _vCountryCode :String
    fileprivate var _vDate :[Double]
    fileprivate var _vTemperature :[String]?
    fileprivate var _vPressure :[String]?
    fileprivate var _vHumidity :[String]?
    fileprivate var _vWeatherCondition = [String]()
    fileprivate var _vSpeed :[String]?
    fileprivate var _vDegree :[Int]?
    fileprivate var _vClouds :[String]?
    fileprivate var _vRain :[String]?
    fileprivate var _vWeatherIcon = [String]()
    
    
    var cityName:String{
        return _vCityName
    }
    var countryCode:String{
        return _vCountryCode
    }
    var date:[Double]{
        return _vDate
    }
    var temperature:[String]?{
        return _vTemperature
    }
    var pressure:[String]?{
        return _vPressure
    }
    var humidty:[String]?{
        return _vHumidity
    }
    var weatherCondition : [String]?{
        return _vWeatherCondition
    }
    var speed:[String]?{
        return _vSpeed
    }
    var degree:[Int]?{
        return _vDegree
    }
    var clouds:[String]?{
        return _vClouds
    }
    var rain:[String]?{
        return _vRain
    }
    var weatherIcon: [String]?{
        return _vWeatherIcon
    }
    
    
    init(json: JSON) {
        // City NAME
        self._vCityName = json["city"]["name"].stringValue
        // Country Code
        self._vCountryCode = json["city"]["country"].stringValue
        // Date
        self._vDate = (json["list"].arrayValue.map({$0["dt"].doubleValue}))
        //Temperature
        self._vTemperature = json["list"].arrayValue.map({$0["temp"]["day"].stringValue})
        //Pressure
        self._vPressure = json["list"].arrayValue.map({$0["pressure"].stringValue})
        //humidity
        self._vHumidity = json["list"].arrayValue.map({$0["humidity"].stringValue})
        //Weather
        for item in  json["list"].arrayValue{
            for innerItem in item["weather"].arrayValue{
                self._vWeatherCondition.append(innerItem["main"].stringValue)
            }
        }
        // Speed
        self._vSpeed = json["list"].arrayValue.map({$0["speed"].stringValue})
        // Degree
        self._vDegree = json["list"].arrayValue.map({$0["deg"].intValue})
        // clouds
        self._vClouds = json["list"].arrayValue.map({$0["clouds"].stringValue})
        // Rain
        self._vRain = json["list"].arrayValue.map({$0["rain"].stringValue})
        // weather icon
        for item in  json["list"].arrayValue{
            for innerItem in item["weather"].arrayValue{
                self._vWeatherIcon.append(innerItem["icon"].stringValue)
            }
        }
    }
}
