//
//  ForecasterVC.swift
//  weatherForecaster
//
//  Created by Emir haktan Ozturk on 01/08/2017.
//  Copyright © 2017 emirhaktan. All rights reserved.
//

import UIKit
import CoreLocation
import ReachabilitySwift
import Firebase
import FirebaseDatabase

class ForecasterVC: UIViewController, CLLocationManagerDelegate {
    
    @IBOutlet var weatherImageVew: UIImageView!
    @IBOutlet var locationLabel: UILabel!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var degreeConditionLabel: UILabel!
    @IBOutlet var crLabel: UILabel!
    @IBOutlet var rainLabel: UILabel!
    @IBOutlet var celciusLabel: UILabel!
    @IBOutlet var windLabel: UILabel!
    @IBOutlet var compassLabel: UILabel!
    @IBOutlet var shareButton: UIButton!
    @IBOutlet var rainActivityIndicator: UIActivityIndicatorView!
    @IBOutlet var windSpeedActivityIndicator: UIActivityIndicatorView!
    @IBOutlet var humidityActivityIndicator: UIActivityIndicatorView!
    @IBOutlet var compassActivityIndicator: UIActivityIndicatorView!
    @IBOutlet var pressureActivityIndicator: UIActivityIndicatorView!
    @IBOutlet var weatherConditionActivityIndicator: UIActivityIndicatorView!
    @IBOutlet var locationActivityIndicator: UIActivityIndicatorView!
    @IBOutlet var weatherImageActivityIndicator: UIActivityIndicatorView!
    
    let colorBlack = UIColor(rgb: 0x333333)
    let colorBlue = UIColor(rgb: 0x2f91f)
    let colorOrange = UIColor(rgb: 0xff8847)
    
    var databaseRef :DatabaseReference?
    
    var locationManager:CLLocationManager!
    
    let reachability = Reachability()
    
    var weather : weatherModel?
    
    //MARK: viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true//Hide Tab Bar to prevent crash while loading data
        self.shareButton.isHidden = true
        self.uiElements()
        
        Auth.auth().signInAnonymously() { (user, error) in // Make anonymous login with Firebase
        }
        
        self.reachabilityChecks()
    }
    
    // MARK:Edit UI elements
    func uiElements(){
        // Start Activity Indicator
        self.startAnimatingActivityIndicators()
        self.shareButton.setTitleColor(colorOrange, for: UIControlState.normal)
        self.degreeConditionLabel.textColor = colorBlue
        self.titleLabel.textColor = colorBlack
        self.crLabel.textColor = colorBlack
        self.rainLabel.textColor = colorBlack
        self.windLabel.textColor = colorBlack
        self.celciusLabel.textColor = colorBlack
        self.compassLabel.textColor = colorBlack
        self.locationLabel.textColor = colorBlack
    }
    
    private func handleWeatherData(location:CLLocation){
        let lat = String(location.coordinate.latitude)
        let long = String(location.coordinate.longitude)
        let urlString = "http://api.openweathermap.org/data/2.5/forecast/daily?lat=\(lat)&lon=\(long)&cnt=7&units=metric&appid=1048010401e00f5285854c369b1c592e"
        
        let apiCall = ApiManager()
        apiCall.loadData(urlString: urlString) { (error:Error?, weatherInfo:weatherModel?) in
            if error == nil {
            self.weather = weatherInfo
            self.fillScreenData(weatherInfo: self.weather!)
                
            self.pushLocationToFirebase(location: location, weatherInfo: self.weather!)
            }else{
            self.callAlertViewController(title: "Error", message: error!.localizedDescription)
            }
        }
    }
    
    func pushLocationToFirebase(location:CLLocation?, weatherInfo:weatherModel?){
        
        self.databaseRef = Database.database().reference()
        if location != nil && weatherInfo != nil {
            let childUpdates = ["/Location/Latitude" : location!.coordinate.latitude,
                                "/Location/Longitude" :location!.coordinate.longitude,
                                "/Location/CityName" : weatherInfo!.cityName,
                                "/Location/Temperature" :weatherInfo!.temperature![0]] as [String : Any]
            self.databaseRef!.updateChildValues(childUpdates)
        }else{
            return
        }
    }
    
    //MARK: Share Action
    @IBAction func socialShareAction(_ sender: Any) {
        let message = "Weather forecast for today : "
        let cityName = self.weather?.cityName
        let temperature = self.convertDoubleToIntString(doubleString: (self.weather?.temperature?[0])!)
        let condition = self.weather?.weatherCondition?[0]
        
        var activityItems:[AnyObject]?
        activityItems = [message as Any as AnyObject, cityName as Any as AnyObject, temperature as Any as AnyObject, condition as Any as AnyObject]
        let controller = UIActivityViewController.init(activityItems: activityItems!, applicationActivities: nil)
        controller.excludedActivityTypes = [UIActivityType.postToVimeo,UIActivityType.openInIBooks,UIActivityType.postToWeibo,UIActivityType.postToFlickr,UIActivityType.print,UIActivityType.assignToContact,UIActivityType.saveToCameraRoll,UIActivityType.addToReadingList]
        
        self.present(controller, animated: true, completion: nil)
    }
    
    //MARK: Fill UI Elements with their data
    func fillScreenData(weatherInfo:weatherModel){
        // Stop Activity Indicator
        self.stopAnimatingActivityIndicators()
        //Using countryCode JSON to compare received country code to get country name
        let apiCallForCountryCode = ApiManager()
        apiCallForCountryCode.loadCountryData { (error:Error?, country:countryCodeModel?) in
            if error == nil {
            // Create Country Code Dictionary
                var dictionary: [String: String] = [:]
                for (index, element) in (country!.countryCode?.enumerated())!
                {
                    dictionary[element] = country?.countryName?[index]
                }
                // "" + location icon + location text
                let attachment = NSTextAttachment()
                attachment.image = UIImage(named: "Current") // get icon and append it to attachment
                let attachmentString = NSAttributedString(attachment: attachment) // attachment string with image
                let mystring = NSMutableAttributedString(string: "") // create string with empty text
                mystring.append(attachmentString) // append image right of this empty text
                let mystring2 = NSMutableAttributedString(string: " " + weatherInfo.cityName + ", \(dictionary["\(weatherInfo.countryCode)"]!)" ) // get location and country name where country name is retrieved by countryJson file
                mystring.append(mystring2) // apend second text to right of the first text "" icon "Location text"
                self.locationLabel.attributedText = mystring
            }
        }
        
        // MARK:Get double Temperature and convert it to Int style
        let intTemperature = self.convertDoubleToIntString(doubleString: weatherInfo.temperature![0])
        // Temperature & Condition
        self.degreeConditionLabel.text = "\(intTemperature) °C | \(weatherInfo.weatherCondition![0]) "
        
        //Print Rain
        if (weatherInfo.rain![0] != "" ){
        self.rainLabel.text = weatherInfo.rain![0] + " mm"
        }else{self.rainLabel.text = "0.0 mm"}
        // Pressure
        let intPressure = self.convertDoubleToIntString(doubleString: weatherInfo.pressure![0])
        self.celciusLabel.text = intPressure + " hPa"
        // Wind Speed
        self.windLabel.text = weatherInfo.speed![0] + " km/h"
        // Humidty
        self.crLabel.text = "%" + weatherInfo.humidty![0]
        // Get wind Direction
        let windDegree = convertDegreesNorthToCardinalDirection(degrees: weatherInfo.degree![0])
        // print Wind Direction
        self.compassLabel.text = windDegree
        // weather Image depending on icon
        self.iconToWeatherImage(iconName: weatherInfo.weatherIcon![0])
        
        self.shareButton.isHidden = false
        self.tabBarController?.tabBar.isHidden = false // Unhide Tab Bar after finish loading data
        
        // pass weatherinfo to ForecastWeek view controller
        let forecastWeekVC = self.tabBarController?.viewControllers?[1] as! ForecastWeekVC
        forecastWeekVC.weatherInfo = self.weather
    }
    
    // MARK:converts Double type String to Int type string
    func convertDoubleToIntString(doubleString:String) -> String{
    let str = doubleString
    let strArray = str.components(separatedBy: ".")
    let intStr = strArray[0]
    return intStr
    }
    
    // MARK:start activity indicators
    func startAnimatingActivityIndicators(){
        self.locationActivityIndicator.startAnimating()
        self.weatherConditionActivityIndicator.startAnimating()
        self.humidityActivityIndicator.startAnimating()
        self.rainActivityIndicator.startAnimating()
        self.pressureActivityIndicator.startAnimating()
        self.windSpeedActivityIndicator.startAnimating()
        self.compassActivityIndicator.startAnimating()
        self.weatherImageActivityIndicator.startAnimating()
    }
    // MARK:stop activity indicators
    func stopAnimatingActivityIndicators(){
        self.locationActivityIndicator.stopAnimating()
        self.weatherConditionActivityIndicator.stopAnimating()
        self.humidityActivityIndicator.stopAnimating()
        self.rainActivityIndicator.stopAnimating()
        self.pressureActivityIndicator.stopAnimating()
        self.windSpeedActivityIndicator.stopAnimating()
        self.compassActivityIndicator.stopAnimating()
        self.weatherImageActivityIndicator.stopAnimating()
    }
    
    
    // MARK:Assign weather Image
    func iconToWeatherImage(iconName:String){
        switch iconName {
        case "01d":
            self.weatherImageVew.image = UIImage(named: "Sun_Big")
        case "02d":
            self.weatherImageVew.image = UIImage(named: "Cloudy_Big")
        case "03d":
            self.weatherImageVew.image = UIImage(named: "Cloudy_Big")
        case "04d":
            self.weatherImageVew.image = UIImage(named: "Cloudy_Big")
        case "09d":
            self.weatherImageVew.image = UIImage(named: "CR")
        case "10d":
            self.weatherImageVew.image = UIImage(named: "CR")
        case "11d":
            self.weatherImageVew.image = UIImage(named: "Lightning_Big")
        case "13d":
            self.weatherImageVew.image = UIImage(named: "Lightning_Big")
        case "01n":
            self.weatherImageVew.image = UIImage(named: "Sun_Big")
        case "02n":
            self.weatherImageVew.image = UIImage(named: "Cloudy_Big")
        case "03n":
            self.weatherImageVew.image = UIImage(named: "Cloudy_Big")
        case "04n":
            self.weatherImageVew.image = UIImage(named: "Cloudy_Big")
        case "09n":
            self.weatherImageVew.image = UIImage(named: "CR")
        case "10n":
            self.weatherImageVew.image = UIImage(named: "CR")
        case "11n":
            self.weatherImageVew.image = UIImage(named: "Lightning_Big")
        case "13n":
            self.weatherImageVew.image = UIImage(named: "Lightning_Big")
        default:
            self.weatherImageVew.image = UIImage(named: "Rain")
        }
    }

    //MARK: Reachability Methods
    fileprivate func reachabilityChecks(){
    
        // Reachabaility checks
        reachability?.whenReachable = { _ in
            DispatchQueue.main.async {
            }
        }
        reachability?.whenUnreachable = { _ in
            DispatchQueue.main.async {
                self.callAlertViewController(title: "No Internet", message: "Please make sure that your device is connected to internet")
            }
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(internetChanged), name: ReachabilityChangedNotification, object: reachability)
        do{
            try reachability?.startNotifier()
        }catch{
            callAlertViewController(title: "Error!", message: "Could not start notifier")
        }
        
    }
    
    func internetChanged(notification:Notification){
        let reachability = notification.object as! Reachability
        if reachability.isReachable{
                DispatchQueue.main.async {
                    self.getUserLocation()
                }
        }else{
            self.callAlertViewController(title: "No Internet", message: "Please make sure that your device is connected to internet")
        }
    }
    
    // MARK:get users current location
    func getUserLocation(){
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.requestLocation()
        }
    }
    
    //MARK: CLLocation Delegate methods
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let userLocation:CLLocation = locations.first{
        handleWeatherData(location: userLocation)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        callAlertViewController(title: "Error on Location", message: "Could not define the location")
    }
    
    //MARK: Alert View Controller
    fileprivate func callAlertViewController(title:String, message:String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: { (action) in
        })
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK:Get the wind Drection
    func convertDegreesNorthToCardinalDirection(degrees: Int) -> String {
        
        let cardinals: [String] = [ "N",
                                    "NE",
                                    "E",
                                    "SE",
                                    "S",
                                    "SW",
                                    "W",
                                    "NW",
                                    "N" ]
        
        let index = Int(round(Double(degrees).truncatingRemainder(dividingBy: 360) / 45))
        
        return cardinals[index]
    }
}
//MARK: RGB Color
extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
}
