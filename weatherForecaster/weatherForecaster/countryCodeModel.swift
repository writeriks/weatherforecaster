//
//  countryCodeModel.swift
//  weatherForecaster
//
//  Created by Emir haktan Ozturk on 05/08/2017.
//  Copyright © 2017 emirhaktan. All rights reserved.
//

import Foundation
import SwiftyJSON

class countryCodeModel{
    fileprivate var _vCountryName = [String]()
    fileprivate var _vCountryCode = [String]()
    
    var countryName:[String]?{
        return _vCountryName
    }
    var countryCode:[String]?{
        return _vCountryCode
    }
    
    
    init(json: JSON) {
        for item in json.arrayValue{
        self._vCountryCode.append(item["code"].stringValue)
        }
        for item in json.arrayValue{
            self._vCountryName.append(item["name"].stringValue)
        }
    }
}
