//
//  ForecastWeekVC.swift
//  weatherForecaster
//
//  Created by Emir haktan Ozturk on 06/08/2017.
//  Copyright © 2017 emirhaktan. All rights reserved.
//

import UIKit

class ForecastWeekVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet var weaterTableView: UITableView!
    @IBOutlet var locationNameLabel: UILabel!
    
    var weatherInfo : weatherModel?
    let colorBlack = UIColor(rgb: 0x333333)
    let colorBlue = UIColor(rgb: 0x2f91f)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.locationNameLabel.textColor = colorBlack
        self.locationNameLabel.font = UIFont(name: "ProximaNova-Semibold", size: self.locationNameLabel.font.pointSize)
        self.locationNameLabel.text = weatherInfo?.cityName
        self.weaterTableView.delegate = self
        self.weaterTableView.dataSource = self
        self.weaterTableView.tableFooterView = UIView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: Convert Double String to Int String
    func convertDoubleToIntString(doubleString:String) -> String{
        let str = doubleString
        let strArray = str.components(separatedBy: ".")
        let intStr = strArray[0]
        return intStr
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.weatherInfo?.temperature?.count)!
    }
    
    let cellID = "cell"
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath) as! WeatherCell
        cell.conditionLabel.textColor = colorBlack
        cell.dayLabel.textColor = colorBlack
        cell.weatherLabel.textColor = colorBlue
        cell.conditionLabel.text = weatherInfo?.weatherCondition?[(indexPath as IndexPath).row]
        let temperature = weatherInfo?.temperature?[(indexPath as IndexPath).row]
        let currentTemperature = self.convertDoubleToIntString(doubleString: temperature!)
        cell.weatherLabel.text = currentTemperature + " °"
        let imageName = weatherInfo?.weatherIcon?[(indexPath as IndexPath).row]
        let image = self.iconToWeatherImage(iconName: imageName!)
        cell.weatherImageView.image = image
        
        let date = weatherInfo?.date[(indexPath as IndexPath).row]
        let weekDay = self.formatDate(dateDouble: date!)
        cell.dayLabel.text = self.getWeekDay(weekday: weekDay!)
        
        return cell
    }
    //MARK: convert date to day of the week
    func formatDate(dateDouble:Double)->Int?{
        let date = NSDate(timeIntervalSince1970:dateDouble)
        let stringDate = String(describing: date)
        let onlyDateString = stringDate.components(separatedBy: " ")
        let formatter  = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        if let todayDate = formatter.date(from: onlyDateString[0]) {
            let myCalendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
            let myComponents = myCalendar.components(.weekday, from: todayDate)
            let weekDay = myComponents.weekday
            return weekDay!
        } else {
            return nil
        }
    }
    //MARK: return the weekday
    func getWeekDay(weekday:Int)-> String?{
        switch weekday {
        case 1:
            return "Sunday"
        case 2:
            return "Monday"
        case 3:
            return "Tuesday"
        case 4:
            return "Wednesday"
        case 5:
            return "Thursday"
        case 6:
            return "Friday"
        case 7:
            return "Saturday"
        default:
            return ""
        }
    }
    //MARK: get the weather icon
    func iconToWeatherImage(iconName:String)-> UIImage{
        switch iconName {
        case "01d":
            return UIImage(named: "Sun_Big")!
        case "02d":
            return UIImage(named: "Cloudy_Big")!
        case "03d":
            return UIImage(named: "Cloudy_Big")!
        case "04d":
            return UIImage(named: "Cloudy_Big")!
        case "09d":
            return UIImage(named: "CR")!
        case "10d":
            return UIImage(named: "CR")!
        case "11d":
            return UIImage(named: "Lightning_Big")!
        case "13d":
            return UIImage(named: "Lightning_Big")!
        case "01n":
            return UIImage(named: "Sun_Big")!
        case "02n":
            return UIImage(named: "Cloudy_Big")!
        case "03n":
            return UIImage(named: "Cloudy_Big")!
        case "04n":
            return UIImage(named: "Cloudy_Big")!
        case "09n":
            return UIImage(named: "CR")!
        case "10n":
            return UIImage(named: "CR")!
        case "11n":
            return UIImage(named: "Lightning_Big")!
        case "13n":
            return UIImage(named: "Lightning_Big")!
        default:
            return UIImage(named: "Rain")!
        }
    }

}
