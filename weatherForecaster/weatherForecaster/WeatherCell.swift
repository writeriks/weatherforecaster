//
//  WeatherCell.swift
//  weatherForecaster
//
//  Created by Emir haktan Ozturk on 06/08/2017.
//  Copyright © 2017 emirhaktan. All rights reserved.
//

import UIKit

class WeatherCell: UITableViewCell {
    
    @IBOutlet var weatherImageView: UIImageView!
    @IBOutlet var dayLabel: UILabel!
    @IBOutlet var conditionLabel: UILabel!
    @IBOutlet var weatherLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
